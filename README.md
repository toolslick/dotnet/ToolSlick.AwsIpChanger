AWS IP Changer
==============

## Description
Manage IP address of AWS Security Groups. This utility enumerates through all of your security groups, looks for all inbound rules with a description that you provided and changes the IP address of these rules to your public IP address.

The main use of this utility is to automatically update your **Office IP** in AWS security groups. This way, you will be able to easily access the corresponding EC2/RDS/etc resources from your machines. Saves time and major PIA of going and manually changing all these IP addresses.

## Usage

1. Install the dotnet global tool

    ```
    dotnet tool install --global ToolSlick.AwsIpChanger --version 1.0.7
    ```

2. Change IP addresses for all inbound rules with the description `Kolkata`.

    ```
    tsawsipcng 'Kolkata'
    ```

You will see and output like

    Changing IPs for all security group rules with description: Kolkata
    New IP: 202.1.211.199/32
    Removing 2 permissions from: Web Server
    Adding 2 permissions to: Web Server