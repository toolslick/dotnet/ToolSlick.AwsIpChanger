using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using Amazon.EC2;
using Amazon.EC2.Model;

namespace ToolSlick.AwsIpChanger
{
    class Program
    {
        static async Task<int> Main(string[] args)
        {
            if (args.Length != 1)
            {
                Console.WriteLine("Usage: tsawsipcng <Description>");
                return -1;
            }

            var desc = args[0];
            Console.WriteLine($"Changing IPs for all security group rules with description: {desc}");

            var request = WebRequest.CreateHttp("https://api.ipify.org/");

            string ip;
            using (var response = request.GetResponse())
            using (var stream = response.GetResponseStream())
            using (var reader = new StreamReader(stream))
                ip = reader.ReadToEnd();

            ip = $"{ip}/32";
            Console.WriteLine($"New IP: {ip}");

            await Change(desc, ip);
            return 0;
        }

        private static async Task Change(string desc, string ip)
        {
            var client = new AmazonEC2Client();
            var securityGroupsResponse = await client.DescribeSecurityGroupsAsync();
            var securityGroups = securityGroupsResponse.SecurityGroups;

            Console.WriteLine($"Found {securityGroups.Count} security groups");

            foreach (var securityGroup in securityGroups)
            {
                var groupName = securityGroup.GroupName;
                Console.WriteLine($"Processing security group: {groupName}");

                var permsToUpdate = new List<IpPermission>();

                foreach (var permission in securityGroup.IpPermissions)
                foreach (var ipRange in permission.Ipv4Ranges)
                    if (ipRange.Description == desc && ipRange.CidrIp != ip)
                    {
                        permsToUpdate.Add(permission);
                        break;
                    }

                if (permsToUpdate.Count <= 0) 
                    continue;

                Console.WriteLine($"Removing {permsToUpdate.Count} permissions from: {groupName}");
                await client.RevokeSecurityGroupIngressAsync(
                    new RevokeSecurityGroupIngressRequest(groupName, permsToUpdate));

                foreach (var ipPermission in permsToUpdate)
                foreach (var range in ipPermission.Ipv4Ranges)
                    if (range.Description == desc)
                        range.CidrIp = ip;

                Console.WriteLine($"Adding {permsToUpdate.Count} permissions to: {groupName}");
                await client.AuthorizeSecurityGroupIngressAsync(new AuthorizeSecurityGroupIngressRequest
                {
                    GroupName = groupName,
                    IpPermissions = permsToUpdate
                });
            }
        }
    }
}
